<!DOCTYPE html>
<html>
<head>
	<title>Sign Up</title>
</head>
<body>
	<h1>
		<b>Buat Account Baru!</b>
	</h1>

	<p>
		<b>Sign Up Form</b>
	</p>
	<form action="/welcome" method="POST">
		@csrf

		<p>First Name : </p>
		<input type="text" name="first_name">
		<p>Last Name :</p>
		<input type="text" name="last_name">

		<p>Gender</p>
		<input type="radio" name="sex" value="male"/>Male<br/>
		<input type="radio" name="sex" value="female"/>Female<br/>
		<input type="radio" name="sex" value="other"/>Other

		<p>Nationality :</p>
		<select id="Nationality" name="Nationality">
			<option value="Indonesian">Indonesia</option>
			<option value="Malaysian">Malaysian</option>
			<option value="Singaporenas">Singaporeans</option>
		</select>

		<p>Language Spoken :</p>
		<input type="checkbox" name="Bahasa Indonesia"/>Bahasa Indonesia<br/>
		<input type="checkbox" name="English"/>English<br/>
		<input type="checkbox" name="Other"/>Other

		<p>Bio :</p>
		<textarea name="message" rows="10" cols="30"></textarea>

		<br>
		<input type="submit" name="submit">

	</form>

</body>
</html>